Necessário para utilização :

- Ter um servidor php local e colocar todos seus arquivos dentro da página www.

- Configurar o arquivo "conexao.php" com seus dados.

$servidor = "seu servidor"

$usuario = "seu usuario do banco"

$senha = "sua senha"

$banco = "banco de dados que irá usar"

- Executar os script.sql que está dentro da pasta "Scripts".

- Para executar basta utilizar o endereço seuservidor/index.php .

-----------------------------------------------------------------------------------------


Funcionalidades do sistema :

- Um cadastro simples de pessoas e cursos, com uma relação de 1..N entre as tabelas.

- Uma consulta básica de cursos.

------------------------------------------------------------------------------------------ 

O que foi feito para construção do sistema :

Utilizei linguagens de programação como HTML, CSS, JavaScript e PHP. Todos de forma mais nativa possível, com algumas excessões que utilizei Jquery.
Apesar de não ser ser uma regra, a utilização das linguagens sem ferramentas auxiliares, segui a recomendação pois achei que como critério de avaliação 
seria interessante mostrar o que já sabia e aprendi nesse aspecto pois quando utilizar uma ferramente auxiliar terei como extrair 100% dela, porque entenderei
 como funciona toda a lógica nela envolvida.
 
 --------------------------------------------------------------------------------------
 Nota:
 
 Não consegui finalizar diversas funcionalidades, que tinha em mente para esse desafio. Sistema está incompleto, creio eu que com cerca de 65% de 
 conclusão, acredito também que precisaria de mais 2 dias para concluir totalmente.

 Agradeço, desde já a oportunidade que vocês estão me dando, como foi dito na entrevista, eu não sou muito familiarizado com front-end porém estou
 encantando com o ambiente. Muito tempo que algo não me prendia como esse projeto me prendeu, foram basicamentes poucas horas de sono essa semana, devido a minha rotina. 
 Me dediquei no meu limite, espero poder em breve nos reecontrarmos.
 
 
 
 Nome: Matheus Dutra Fonseca
 Idade: 23 anos