function adicionaEvento(psObjeto, psEvento, psFuncao)
{
	if (psObjeto.addEventListener)
	{
		psObjeto.addEventListener(psEvento, psFuncao);
	}
	else if (psObjeto.attachEvent)
	{
		psObjeto.attachEvent("on"+psEvento, psFuncao);
	}
}

function loadDoc(psUrl,psParametros)
{
	
  var xhttp = new XMLHttpRequest();
  
  xhttp.open("POST", psUrl, true);

	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.setRequestHeader("Cache-Control", "no-cache");
  
  xhttp.onreadystatechange = function()
  {
    if (xhttp.readyState == 4 && xhttp.status == 200)
	{
      //document.getElementById("demo").innerHTML =
      alert(xhttp.responseText);
	  window.location.href = window.location.href;
    }
  };
  
  xhttp.send(psParametros);
  
}

function selecionaBolinha()
{
	for(var i=0;i<this.parentNode.getElementsByTagName("li").length;i++)
	{
		this.parentNode.getElementsByTagName("li")[i].className="bolinha-desmarcada";
	}
	this.className = "bolinha-marcada"; 
}

function alteraImagemBolinha()
{
	var iIndice = 0;
	for(var i=0; i < this.parentNode.getElementsByTagName("li").length; i++)
	{
		if(this.parentNode.getElementsByTagName("li")[i]==this)
		{
			iIndice = i;
		}
	}
	
	for(i=0; i < document.getElementById("imgBanner").getElementsByTagName("img").length; i++)
	{
		if(iIndice==i)
		{
			document.getElementById("imgBanner").getElementsByTagName("img")[i].style.display="inline";
		}
		else
		{
			document.getElementById("imgBanner").getElementsByTagName("img")[i].style.display="none";
		}
	}
}

function alteraImagemSeta()
{
	var aImgs = document.getElementById("imgBanner").getElementsByTagName("img");
	
	var tam = document.getElementById("imgBanner").getElementsByTagName("img").length;

	var iIndice = 0;

	
	for(var i=0; i < document.getElementById("imgBanner").getElementsByTagName("img").length; i++)
	{
		if(this.id=="setaDireita")
		{
			if(document.getElementById("imgBanner").getElementsByTagName("img")[i].style.display=="inline")
			{
				if(document.getElementById("imgBanner").getElementsByTagName("img")[i+1])
				{
					iIndice = i+1;
					
				}
				else
				{	
					iIndice = tam-1;
				}
			}
		}
		else
		{
			if(document.getElementById("imgBanner").getElementsByTagName("img")[i].style.display=="inline")
			{
				if(document.getElementById("imgBanner").getElementsByTagName("img")[i-1])
				{
					iIndice = i-1;
				}
			}
		}
		document.getElementById("imgBanner").getElementsByTagName("img")[i].style.display="none";
		
	}
	
	document.getElementById("selecionaImagens").getElementsByTagName("li")[iIndice].click();	
}

//função para mudar imagem com as setas do banner 

function carregaEventosSeta()
{
	adicionaEvento(document.getElementById("setaDireita"),"click",alteraImagemSeta);
	adicionaEvento(document.getElementById("setaEsquerda"),"click",alteraImagemSeta);
}

//função para chamadas dos das setas

function carregaEventosBolinha()
{
	var oUl = document.getElementById("selecionaImagens");
	for(var i=0; i < oUl.getElementsByTagName("li").length; i++)
	{
		
		adicionaEvento(oUl.getElementsByTagName("li")[i],"click",selecionaBolinha);
		adicionaEvento(oUl.getElementsByTagName("li")[i],"click",alteraImagemBolinha);
		adicionaEvento(oUl.getElementsByTagName("li")[i],"click",desabilitaSetas);
		
		
	}
}

function desabilitaSetas()
{
	
	var tam = document.getElementById("imgBanner").getElementsByTagName("img").length;

	for(var i=0; i < tam; i++)
	{
		if(document.getElementById("imgBanner").getElementsByTagName("img")[i].style.display=="inline")
			{
				if (i == (tam-1))
				{
					document.getElementById("setaDireita").className="setaDesabilitada";	
					
				}else
				{
					if (i == 0)
					{
						document.getElementById("setaEsquerda").className="setaDesabilitada";				
						
					}	else
						{
						  document.getElementById("setaEsquerda").className="setaHabilitada";	
						  document.getElementById("setaDireita").className="setaHabilitada";
						}
				}
			
		
			}
	}
	
	
}


// Cookies
function criaCookie(psNome, psValor, sDias)
{
	var oExpira = "";
	
	if (sDias)
	{
		var oData = new Date();
		oData.setTime(oData.getTime() + (sDias * 24 * 60 * 60 * 1000));
		var oExpira = "; expires=" + oData.toGMTString();
	}              

	document.cookie = psNome + "=" + psValor + oExpira + "; path=/";
}

function leCookie(psNome)
{
	var aCookie = document.cookie.split(';');
	for (var i = 0; i < aCookie.length; i++)
	{
		var oCookie = aCookie[i];
		while (oCookie.charAt(0) == ' ') oCookie = oCookie.substring(1, oCookie.length);
		if (oCookie.indexOf(psNome+"=") == 0) return oCookie.substring(psNome+"=".length, oCookie.length);
	}
	return null;
}

function verificaCookie(){	
		if(leCookie("nome_usuario_leo")==null && leCookie("login_usuario_leo")==null)
		{
			$("#ex").click();

		}
}

function trocaImg(eve){
	

	var sNomeImg = eve.target.src.split("\/")[eve.target.src.split("\/").length-1];	
	var sNomeDir = eve.target.src.replace(sNomeImg,"")+"\/";	
	var sNomeExt = sNomeImg.split(".")[1];
	var sNomeSemExt = sNomeImg.split(".")[0];
	
	if (eve.type == "mouseover"){
		eve.target.src = sNomeDir+sNomeSemExt+"_col"+"."+sNomeExt	
	}else{
		eve.target.src = sNomeDir+sNomeSemExt.replace("_col","")+"."+sNomeExt
	}
		
}

function carregaTrocaImg(){	
	adicionaEvento(document.getElementById("twitter"),"mouseover",function(){trocaImg(event)});
	adicionaEvento(document.getElementById("twitter"),"mouseout",function(){trocaImg(event)});
	adicionaEvento(document.getElementById("pinterest"),"mouseover",function(){trocaImg(event)});
	adicionaEvento(document.getElementById("pinterest"),"mouseout",function(){trocaImg(event)});
	adicionaEvento(document.getElementById("youtube"),"mouseover",function(){trocaImg(event)});
	adicionaEvento(document.getElementById("youtube"),"mouseout",function(){trocaImg(event)});	
}

function carregaClickCriarCurso(){
	adicionaEvento(document.getElementById("criarCurso"),"click",criarCurso);
}

function retornaElemento()
{
	var oElemento=null;
	if(this.id.toLowerCase().indexOf("p")!=-1)
	{
		var oElemento = document.createElement("p");
		oElemento.id = "criaDetalheCurso";
		oElemento.className = "txtDetalheCurso";
		oElemento.innerHTML = (this.value!="")?this.value:"Clique aqui para editar a descrição do curso aqui.";
	}
	else
	{
		oElemento = document.createElement("h6");
		oElemento.id="criaTituloCurso";
		oElemento.innerHTML =(this.value!="")?this.value:"Clique para editar o título";
	}
	this.parentNode.insertBefore(oElemento,this);
	this.parentNode.removeChild(this);
	adicionaEvento(oElemento,"click",editarCampos);
}

function editarCampos(){
	var oInput = document.createElement("input");
	oInput.type = "text";
	oInput.className = this.className;
	oInput.placeholder = this.innerHTML;
	oInput.id = this.tagName + "_editado";
	adicionaEvento(oInput,"blur",retornaElemento);
	this.parentNode.insertBefore(oInput,this);
	this.parentNode.removeChild(this);
	oInput.focus();
	
}


function criarCurso(){
	
	var oButton = document.createElement("button");
		oButton.type="submit";
		oButton.className="btnVerde botaoVerCurso";
		oButton.id="botaoSalvar";
		oButton.innerHTML="Salvar";
	var oForm = document.createElement("form");
		oForm.id="frmCriaCurso";
		oForm.className="frmVerCurso";
	var oP = document.createElement("p");
		oP.id = "criaDetalheCurso";
		oP.className = "txtDetalheCurso";
		oP.innerHTML = "Clique aqui para editar a descrição do curso aqui.";
	var oH6 = document.createElement("h6");
		oH6.id="criaTituloCurso";
		oH6.innerHTML ="Clique para editar o título";
	var oDivCorpoCriaCurso = document.createElement("div");
		oDivCorpoCriaCurso.className = "cursoCadastardo";
	var oDivImgCriaCurso = document.createElement("div");
		oDivImgCriaCurso.id = "criaImgCurso";
		oDivImgCriaCurso.className = "imgCurso bordaPontilhada";
		oDivImgCriaCurso.innerHTML = "Clique aqui para adicionar uma imagem."
	var oDivContCriaCurso = document.createElement("div");
		oDivContCriaCurso.className = "conteudoCurso";
	
	oForm.appendChild(oButton);
	oDivContCriaCurso.appendChild(oH6);
	oDivContCriaCurso.appendChild(oP);
	oDivContCriaCurso.appendChild(oForm);
	oDivCorpoCriaCurso.appendChild(oDivImgCriaCurso);
	oDivCorpoCriaCurso.appendChild(oDivContCriaCurso);
	this.parentNode.insertBefore(oDivCorpoCriaCurso,this);
	
	adicionaEvento(oP,"click",editarCampos);
	adicionaEvento(oH6,"click",editarCampos);				
}

function main()
{
	carregaEventosBolinha();
	carregaEventosSeta();
	carregaTrocaImg();
	carregaClickCriarCurso();
	verificaCookie();

}

main();
