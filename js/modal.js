function adicionaEvento(psObjeto, psEvento, psFuncao)
{
	if (psObjeto.addEventListener)
	{
		psObjeto.addEventListener(psEvento, psFuncao);
	}
	else if (psObjeto.attachEvent)
	{
		psObjeto.attachEvent("on"+psEvento, psFuncao);
	}
}

$( document ).ready(function()
{
	$('#cadastro').modal({
	closeExisting: false
	});

	$('#loginModal').modal({
		closeExisting: false
	});

	$('#modalInicial').modal({
		
	});
}};





function preencheCookie()
{
	if(document.getElementById("nome").value!="" && document.getElementById("login").value!="" &&  document.getElementById("senha").value!="")
	{
		criaCookie("nome_usuario_leo", document.getElementById("nome").value, 7);
		criaCookie("login_usuario_leo", document.getElementById("login").value, 7);
		$('#closeModalCadastro').click();
	}
}

function carregaClickBtnModal(){
	adicionaEvento(document.getElementById("btnModalCad"),"click",cliqueBotaoBtnModal);	
}

function carregaClickBtnModal2(){
	adicionaEvento(document.getElementById("logarLogout"),"click",cliqueBotaoBtnModal2);	
}

function cliqueBotaoBtnModal(){
	document.getElementById("linkModal").click();
}

function cliqueBotaoBtnModal2(){
	document.getElementById("ex2").click();
}

function logarUsuario()
{
	
	var sLogin = document.getElementById("logarLogin").value;	
	var sSenha = document.getElementById("logarSenha").value;
	
	if(sLogin!="" && sSenha!="")
	{
		loadDoc("autenticacao.php","login="+sLogin+"&senha="+sSenha);
	}
	else
	{
		alert("Favor preenher corretamente os campos,");
	}
	
}

function carregaClickLogar()
{
	adicionaEvento(document.getElementById("logar"),"click",logarUsuario);
}

function salvarUsuario()
{
	var sNome = document.getElementById("nome").value;
	var sLogin = document.getElementById("login").value;
	var sSenha = document.getElementById("senha").value;
	if(sNome!="" && sLogin!="" && sSenha!="")
	{
		loadDoc("cadastrar.php","nome="+sNome+"&login="+sLogin+"&senha="+sSenha);
	}
	else
	{
		alert("Favor preenher corretamente os campos,");
	}
	
}

function carregaClickSalvar()
{
	adicionaEvento(document.getElementById("salvar"),"click",salvarUsuario);
}

adicionaEvento(document.getElementById("salvar"),"click", preencheCookie);
carregaClickSalvar();
carregaClickLogar();