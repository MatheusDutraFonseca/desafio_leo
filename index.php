<!DOCTYPE html>
<?php
	session_start();
		require_once("conexao.php");
		if (!empty($_SESSION['nome']) and !empty($_SESSION['login'])){
			$nome=$_SESSION['nome'];
			$nome=$_SESSION['login'];
		
		}
?>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="css/1024.css">
		<link rel="stylesheet" href="css/jquery.modal.min.css" />

	</head>
	<body>
		<div id="principal">
		<!-- parte superior do layout -->
			<div id="topo">			
				<div id="painelSup">
					<div id="logoLeo">
					</div>				
					<div id="cantoDireito">		
						<form id="frmBusca">
							<div id="areaBusca">
								<input type="text" id="campoBuscar" placeholder="Pesquisar cursos..."/>
								<button type="submit" id="botaoBuscar"></button>
							</div>
						</form>
						<div id="areaLogado">
							<div class="corpoFotoPerfil">
								<img src="css/img/1024/foto_perfil.png" id="fotoPerfil">
							</div>
							<div id="infoPerfil">
							<p id="textoBoasVindas">Você está desconectado</p>
							<p id="nomeUsuario" class="negrito"></p>
							<button type="button" id="logarLogout"  class="btnLogarLogout">Logar</button>
							<a href="modal.php" id="ex2" rel="modal:open"></a>
							</div>
						</div>						
					</div>		
				</div>
				<div id="banner">
					<div id="corpoBanner">
					<div id ="imgBanner">
						
						<img style="display:none;" src="css/img/1024/banner1.jpg">
						<img style="display:inline;" src="css/img/1024/banner2.jpg">
						<img style="display:none;" src="css/img/1024/banner3.jpg">
						
					</div>					
						<div id="dialogBanner">
						
							<h1>LOREM IPSUM</h1>
							<p class='quebraLinhas textoDialogBanner'>Dolor sit amet, consectetur adipiscing elit. Nulla posuere euismod purus sed imperdiet.Cras vel purus posuere, dignissim quam vel, cursus nulla.</p>
							<form id="frmVisualizarCurso">
								<button type="submit" id="botaoVisualizarCurso">VER CURSO</button>
							</form>
						</div>
					<div id="setaDireita" class="setaHabilitada">
						&rsaquo;
						</div>
					<div id="setaEsquerda" class="setaHabilitada">
						&lsaquo;
					</div>						
					</div>	
				</div>
				<div id="marcadorBanner">
				<ul id="selecionaImagens" class="marcador-bolinha">
					<li class="bolinha-desmarcada">&nbsp;</li>
					<li class="bolinha-marcada">&nbsp;</li>
					<li class="bolinha-desmarcada">&nbsp;</li>
				</ul>
				</div>		
			</div>
			<!-- parte central do conteudo -->
			<div id="corpo">
				<p class="negrito">MEUS CURSOS</p>
				<div class"corpoCursos">
					<div class="cursoCadastardo">
						<div class="imgCurso"><img src="css/img/1024/capa_curso.png" class="imgCurso"></div>
							<div class="conteudoCurso">
								<h6>pellentesque malesuada</h6>
								<p class="txtDetalheCurso">Suspendisse sit amet velit neque. Quisque sagittis diam ac lectus consequat.</p>
								<form class="frmVerCurso">
								<button type="submit" class="botaoVerCurso btnVerde">ver curso</button>
								</form>
							</div>
					</div>
					<div id="criarCurso">
						<IMG SRC="css/img/1024/img_cria_curso.png" />
						<p id="txt_adc">  ADICIONAR</p>
						<p id="txt_curso">  CURSO</p>

					</div>
				</div>
			</div>
			<!-- parte inferior do layout-->
			<footer>
			<div id="rodape">
				<IMG id="logoLeoRodape" SRC="css/img/1024/log_leo_rodape.png" />
				<p class="textoRodape quebraLinhas" id="txtLogo" >Phasellus tempus velit at odio iaculis gravida. Donec aliquam, erat non dictum laoreet, odio.</p>
				<div id="contatos">
				<p class="titulosContatos" id="cont">// contatos</p>
				<pre id="txtcontato" class="textoRodape">(21)98765-3434
contato@leolearning.com
				</pre>

					<div id="redesSociais">
						<nav>
						<p class="titulosContatos">// redes socias</p>
							<ul>
								<li><a href="https://twitter.com/leolearning"><img id="twitter" class="iconRS " src="css/img/1024/icon_twitter.png" /></a></li>
								<li><a href="https://www.youtube.com/leolearning"><img id="youtube" class="iconRS" src="css/img/1024/icon_youtube.png" /></a></li>
								<li><a href="https://br.pinterest.com/"><img id="pinterest" class="iconRS" src="css/img/1024/icon_pinterest.png" /></a></li>
							</ul>
						</nav>
					</div>
				</div>
				<div id ="rodape2">
				<p>Copyright 2017 - All right reserved.</p>
				</div>
			</div>
			</footer>
		</div>
		
		<a href="modal.php" id="ex" rel="modal:open"></a>
		<!-- scripts-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.modal.min.js"></script>
		<script src="js/modal.js"></script>
		<script src="js/javascript.js"></script>
		
	</body>
</html> 