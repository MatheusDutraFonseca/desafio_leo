CREATE DATABASE desafioleo;

CREATE TABLE `desafioleo`.`curso` ( `id_curso` INT NOT NULL , `nome` VARCHAR(20) NOT NULL , `descricao` VARCHAR(200) NOT NULL , `img` VARCHAR(200) NOT NULL , PRIMARY KEY (`id_curso`)) ENGINE = MyISAM;

CREATE TABLE `desafioleo`.`curso_usua` ( `id_curso` INT NOT NULL , `id_usua` INT NOT NULL , PRIMARY KEY (`id_curso`, `id_usua`)) ENGINE = MyISAM;

CREATE TABLE `desafioleo`.`usuarios` ( `id_usua` INT NOT NULL , `login` VARCHAR(50) NOT NULL , `senha` VARCHAR(20) NOT NULL , `nome` VARCHAR(50) NOT NULL , PRIMARY KEY (`id_usua`)) ENGINE = MyISAM;
